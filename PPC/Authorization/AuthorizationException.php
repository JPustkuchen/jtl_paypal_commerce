<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Authorization;

use Exception;

/**
 * Class AutorizationException
 * @package Plugin\jtl_paypal_commerce\PPC\Authorization
 */
class AuthorizationException extends Exception
{
}
