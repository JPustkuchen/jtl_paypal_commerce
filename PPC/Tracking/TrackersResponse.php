<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Tracking;

use JsonException;
use Plugin\jtl_paypal_commerce\PPC\Request\ClientError;
use Plugin\jtl_paypal_commerce\PPC\Request\JSONResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;

/**
 * Class TrackersResponse
 * @package Plugin\jtl_paypal_commerce\PPC\Tracking
 */
class TrackersResponse extends JSONResponse
{
    /**
     * @return TrackerIdentifier[]
     * @throws UnexpectedResponseException
     */
    public function getTrackerIdentifiers(): array
    {
        try {
            return \array_map(static function (object $item) {
                return new TrackerIdentifier($item);
            }, $this->getData()->tracker_identifiers ?? []);
        } catch (JsonException $e) {
            throw new UnexpectedResponseException($this, $this->getExpectedResponseCode(), $e);
        }
    }

    /**
     * @return ClientError[]
     * @throws UnexpectedResponseException
     */
    public function getErrors(): array
    {
        try {
            return \array_map(static function (object $item) {
                return new ClientError($item);
            }, $this->getData()->errors ?? []);
        } catch (JsonException $e) {
            throw new UnexpectedResponseException($this, $this->getExpectedResponseCode(), $e);
        }
    }
}
