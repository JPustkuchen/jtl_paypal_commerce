<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Tracking;

use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class TrackerIdentifier
 * @package Plugin\jtl_paypal_commerce\PPC\Tracking
 */
class TrackerIdentifier extends JSON
{
    /**
     * TrackerIdentifier constructor
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[]);
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->getData()->transaction_id ?? '';
    }

    /**
     * @param string $transaction_id
     * @return self
     */
    public function setTransactionId(string $transaction_id): self
    {
        $this->data->transaction_id = $transaction_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrackingNumber(): string
    {
        return $this->getData()->tracking_number ?? '';
    }

    /**
     * @param string $tracking_number
     * @return self
     */
    public function setTrackingNumber(string $tracking_number): self
    {
        $this->data->tracking_number = $tracking_number;

        return $this;
    }
}
