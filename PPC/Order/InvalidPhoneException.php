<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use InvalidArgumentException;

/**
 * Class InvalidPhoneException
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class InvalidPhoneException extends InvalidArgumentException
{
}
