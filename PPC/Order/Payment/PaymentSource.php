<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order\Payment;

use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class PaymentSource
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class PaymentSource extends Payer
{
    /**
     * @param string|string[]|object $data
     * @return string|string[]|object|null
     */
    private function mapData($data)
    {
        $data->billing_address = $data->address ?? null;
        unset($data->address);

        $data->email = $data->email_address ?? null;
        unset($data->email_address);

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function setData($data)
    {
        parent::setData($data);

        $expContext = $this->getData()->experience_context ?? null;
        if ($expContext !== null && !($expContext instanceof ExperienceContext)) {
            $this->setExperienceContext((new ExperienceContext($expContext)));
        }

        $address = $this->getData()->billing_address ?? null;
        if ($address !== null && !($address instanceof Address)) {
            $this->setAddress((new Address($address)));
        }

        $bankDetails = $this->getData()->deposit_bank_details ?? null;
        if ($bankDetails !== null && !($bankDetails instanceof BankDetails)) {
            $this->setBankDetails((new BankDetails($bankDetails)));
        }

        $attributes = $this->getData()->attributes ?? null;
        if ($attributes !== null && !($attributes instanceof JSON)) {
            $this->setAttributes((new JSON($attributes)));
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return $this->mapData(clone parent::getData());
    }

    /**
     * @param Payer $payer
     * @return $this
     */
    public function applyPayer(Payer $payer): self
    {
        $payerData = clone $payer->getData();
        $this->setData($payerData)->setPhone($payer->getPhone());

        return $this;
    }

    /**
     * @param ExperienceContext|null $context
     * @return self
     */
    public function setExperienceContext(?ExperienceContext $context): self
    {
        if ($context === null) {
            unset($this->data->experience_context);
        } else {
            $this->data->experience_context = $context;
        }

        return $this;
    }

    /**
     * @return ExperienceContext|null
     */
    public function getExperienceContext(): ?ExperienceContext
    {
        return $this->data->experience_context ?? null;
    }

    /**
     * @inheritDoc
     */
    public function setPhone(?Phone $phone): Payer
    {
        $this->data->phone = $phone;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPhone(): Phone
    {
        return $this->data->phone;
    }

    /**
     * @param string $paymentReference
     * @return self
     */
    public function setPaymentRefernce(string $paymentReference): self
    {
        $this->data->payment_reference = $paymentReference;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentReference(): string
    {
        return $this->data->payment_reference ?? '';
    }

    /**
     * @param BankDetails|null $bankDetails
     * @return $this
     */
    public function setBankDetails(?BankDetails $bankDetails): self
    {
        if ($bankDetails === null) {
            unset($this->data->deposit_bank_details);
        } else {
            $this->data->deposit_bank_details = $bankDetails;
        }

        return $this;
    }

    /**
     * @return BankDetails
     */
    public function getBankDetails(): BankDetails
    {
        return $this->data->deposit_bank_details ?? new BankDetails();
    }

    /**
     * @param CardDetails|null $cardDetails
     * @return self
     */
    public function setCardDetails(CardDetails $cardDetails): self
    {
        foreach ($cardDetails->getData() as $detailName => $cardDetail) {
            if (empty($cardDetail)) {
                unset($this->data->$detailName);
            } else {
                $this->data->$detailName = $cardDetail;
            }
        }

        return $this;
    }

    /**
     * @return CardDetails
     */
    public function getCardDetails(): CardDetails
    {
        return new CardDetails($this->getData());
    }

    /**
     * @param JSON $attributes
     * @return self
     */
    public function setAttributes(JSON $attributes): self
    {
        $data = $attributes->getData();

        if ($data === null) {
            unset($this->data->attributes);
        } else {
            $this->data->attributes = (object)[];
            foreach ($data as $name => $object) {
                $this->addAttribute($name, $data instanceof JSON ? $data : new JSON($data));
            }
        }

        return $this;
    }

    /**
     * @param string              $name
     * @param JSON $attribute
     * @return self
     */
    public function addAttribute(string $name, JSON $attribute): self
    {
        if (!isset($this->data->attributes)) {
            $this->data->attributes = (object)[];
        }
        $this->data->attributes->$name = $attribute;

        return $this;
    }

    /**
     * @return JSON|null
     */
    public function getAttributes(): ?JSON
    {
        return $this->getData()->attributes ?? null;
    }

    /**
     * @param string $name
     * @return JSON|null
     */
    public function getAttribute(string $name): ?JSON
    {
        $data = $this->getAttributes();
        if ($data === null) {
            return null;
        }

        return $data->$name ?? null;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->experience_context)
            || ($data->experience_context instanceof SerializerInterface && $data->experience_context->isEmpty())
        ) {
            unset($data->experience_context);
        }
        if (empty($data->billing_address)
            || ($data->billing_address instanceof SerializerInterface && $data->billing_address->isEmpty())
        ) {
            unset($data->billing_address);
        }
        if (empty($data->deposit_bank_details)
            || ($data->deposit_bank_details instanceof SerializerInterface && $data->deposit_bank_details->isEmpty())
        ) {
            unset($data->deposit_bank_details);
        }

        return $data;
    }
}
