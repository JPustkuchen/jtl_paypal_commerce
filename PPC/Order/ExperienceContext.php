<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class ExperienceContext
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class ExperienceContext extends JSON
{
    /**
     * ExperienceContext constructor.
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[
            'locale'                        => 'de-DE',
            'customer_service_instructions' => [],
        ]);
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->data->locale;
    }

    /**
     * @param string $locale
     * @return self
     */
    public function setLocale(string $locale): self
    {
        if ($locale !== 'de-DE') {
            throw new InvalidArgumentException(\sprintf('%s is not a valid locale', $locale));
        }

        $this->data->locale = $locale;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBrandName(): ?string
    {
        return $this->data->brand_name ?? null;
    }

    /**
     * @param string|null $brandName
     * @return self
     */
    public function setBrandName(?string $brandName): self
    {
        if ($brandName === null) {
            unset($this->data->brand_name);
        } else {
            $this->data->brand_name = PPCHelper::shortenStr($brandName, 127);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogoURL(): ?string
    {
        return $this->data->logo_url ?? null;
    }

    /**
     * @param string|null $logoURL
     * @return self
     */
    public function setLogoURL(?string $logoURL): self
    {
        if ($logoURL === null) {
            unset($this->data->logo_url);
        } else {
            $this->data->logo_url = $logoURL;
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getCustomerServiceInstructions(): ?array
    {
        return count($this->data->customer_service_instructions) === 0
            ? null
            : $this->data->customer_service_instructions;
    }

    /**
     * @param array $instructions
     * @return self
     */
    public function setCustomerServiceInstructions(array $instructions): self
    {
        $this->data->customer_service_instructions = $instructions;

        return $this;
    }

    /**
     * @param string $instruction
     * @return $this
     */
    public function addCustomerServiceInstruction(string $instruction): self
    {
        $this->data->customer_service_instructions[] = $instruction;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->logo_url)) {
            unset($data->logo_url);
        }
        if (empty($data->brand_name)) {
            unset($data->brand_name);
        }
        if (empty($data->customer_service_instructions)
            || (\is_array($data->customer_service_instructions) && count($data->customer_service_instructions) === 0)) {
            unset($data->customer_service_instructions);
        }

        return $data;
    }
}
