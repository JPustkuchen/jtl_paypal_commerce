<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC;

/**
 * Class PPCHelper
 * @package Plugin\jtl_paypal_commerce\PPC
 */
class PPCHelper
{
    /**
     * @param string $str
     * @param int    $maxLen
     * @param string $shortener
     * @return string
     */
    public static function shortenStr(string $str, int $maxLen, string $shortener = '...'): string
    {
        $strLen = \mb_strlen($str);
        if ($strLen <= $maxLen || $maxLen <= 0) {
            return $str;
        }

        $newStr = \mb_substr($str, 0, $maxLen - \mb_strlen($shortener));

        return $newStr . $shortener;
    }
}
