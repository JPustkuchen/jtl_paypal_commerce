<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\CronJob;

use DateTime;
use JTL\Alert\Alert;
use JTL\Cron\Admin\Controller;
use JTL\Cron\JobInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;

/**
 * Class CronHelper
 * @package Plugin\jtl_paypal_commerce\CronJob
 */
final class CronHelper
{
    public const CRON_TYPE = 'plugin:jtl_paypal_commerce';

    private const SETTING_NAME = BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL . '_shipmenttracking';

    /** @var self */
    private static $instance;

    /** @var Configuration */
    private $config;

    /**
     * CronHelper constructor
     */
    private function __construct(Configuration $config)
    {
        $this->config   = $config;
        self::$instance = $this;
    }

    /**
     * @param Configuration $config
     * @return self
     */
    public static function getInstance(Configuration $config): self
    {
        return self::$instance ?? new self($config);
    }

    /**
     * @return void
     */
    public static function createCron(): void
    {
        /** @var Controller $controller */
        $controller = Shop::Container()->get(Controller::class);
        $alertSrvc  = Shop::Container()->getAlertService();
        $cron       = self::getCron($controller);
        if ($cron !== null) {
            return;
        }

        if ($controller->addQueueEntry([
            'type'      => self::CRON_TYPE,
            'frequency' => '6',
            'time'      => '02:00',
            'date'      => (new DateTime())->format('Y-m-d H:i:s'),
        ]) <= 0) {
            $alertSrvc->addAlert(
                Alert::TYPE_ERROR,
                \__('Der Cron-Job für den Sendungsstatus konnte nicht erstellt werden'),
                'cronCreation'
            );
        }
    }

    /**
     * @return void
     */
    public static function dropCron(): void
    {
        /** @var Controller $controller */
        $controller = Shop::Container()->get(Controller::class);
        $alertSrvc  = Shop::Container()->getAlertService();
        $cron       = self::getCron($controller);
        if ($cron === null) {
            return;
        }

        if ($controller->deleteQueueEntry($cron->getCronID()) <= 0) {
            $alertSrvc->addAlert(
                Alert::TYPE_ERROR,
                \__('Der Cron-Job für den Sendungsstatus konnte nicht gelöscht werden'),
                'cronCreation'
            );
        }
    }

    /**
     * @param Controller $controller
     * @return JobInterface|null
     */
    private static function getCron(Controller $controller): ?JobInterface
    {
        $cron = \array_filter($controller->getJobs(), static function (JobInterface $job) {
            return $job->getType() === self::CRON_TYPE;
        });

        return count($cron) === 0 ? null : \array_shift($cron);
    }

    /**
     * @param array $args
     * @return void
     */
    public function mappingCronjobType(array &$args): void
    {
        /** @var string $type */
        $type = $args['type'];
        if ($type === self::CRON_TYPE) {
            $args['mapping'] = CronJob::class;
        }
    }

    /**
     * @param array $args
     * @return void
     */
    public function availableCronjobType(array &$args): void
    {
        if (!\in_array(self::CRON_TYPE, $args['jobs'], true)
            && $this->config->getPrefixedConfigItem(self::SETTING_NAME, 'N') === 'Y'
        ) {
            $args['jobs'][] = self::CRON_TYPE;
        }
    }
}
