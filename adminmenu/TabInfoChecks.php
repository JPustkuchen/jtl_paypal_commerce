<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use JTL\Backend\NotificationEntry;
use JTL\Helpers\Text;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Configuration;

/**
 * Class TabInfoChecks
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
class TabInfoChecks
{
    /** @var PluginInterface */
    private $plugin;

    /**
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     * @param int $methodID
     * @return bool
     */
    public function isConnectable(int $methodID): bool
    {
        if (!Shop::Container()->get(Configuration::class)->isAuthConfigured()) {
            return false;
        }

        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromID($methodID);

        return $payMethod !== null && $payMethod->isValidIntern([
            'doOnlineCheck'       => true,
            'checkConnectionOnly' => true,
        ]);
    }

    /**
     * @param int      $methodID
     * @param string[] $info
     * @param string   $wrapper
     * @return string
     */
    public function getConnectionInfo(int $methodID, array $info = [], string $wrapper = '%s %s'): string
    {
        if ($this->isConnectable($methodID)) {
            return $info[NotificationEntry::TYPE_NONE] ?? '';
        }

        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromID($methodID);
        if ($payMethod !== null) {
            $notification = $payMethod->getBackendNotification($this->plugin, true);
            if ($notification !== null && $notification->hasDescription()) {
                return \sprintf(
                    $wrapper,
                    Text::htmlentitiesOnce($notification->getDescription()),
                    $info[$notification->getType()] ?? ''
                );
            }
        }

        return $info[NotificationEntry::TYPE_DANGER] ?? '';
    }

    /**
     * @param int      $methodID
     * @param string[] $info
     * @param string   $wrapper
     * @return string
     */
    public function isShippmentLinked(int $methodID, array $info = [], string $wrapper = '%s %s'): string
    {
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromID($methodID);
        if ($payMethod !== null) {
            if (!$payMethod->isAssigned()) {
                return \sprintf(
                    $wrapper,
                    Text::htmlentitiesOnce(\__('Konfiguration mit Versandart fehlt')),
                    $info[NotificationEntry::TYPE_DANGER] ?? ''
                );
            }

            $notification = $payMethod->getBackendNotification($this->plugin, true);
            if ($notification !== null && $notification->hasDescription()) {
                return \sprintf(
                    $wrapper,
                    Text::htmlentitiesOnce($notification->getDescription()),
                    $info[$notification->getType()] ?? ''
                );
            }

            return $info[NotificationEntry::TYPE_NONE] ?? '';
        }

        return $info[NotificationEntry::TYPE_DANGER] ?? '';
    }
}
