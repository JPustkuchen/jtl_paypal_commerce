<div id="shipmentState" class="container-fluid">
    <div class="d-flex justify-content-start align-items-center">
        <div class="subheading1">
            {__('Sendungsinformationen übermitteln')}
        </div>
    </div>
    <hr class="mb-3">
    <form method="post" enctype="multipart/form-data" class="shipmentState">
        {$jtl_token}
        <input type="hidden" name="kPlugin" value="{$kPlugin}" />
        <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
        <input type="hidden" name="id" value="" id="deleteID" />

        {if count($mappingItems) > 0}
        <div class="table-responsive card-body">
            <span class="subheading1">{__('Carrier Mapping')}</span>
            <hr class="mb-3">
            <table class="list table table-striped table-align-top">
                <thead>
                <tr>
                    <th class="th-2">{__('Carrier in JTL-Wawi')}</th>
                    <th class="th-3">{__('Carrier bei PayPal')}</th>
                    <th class="th-4"></th>
                </tr>
                </thead>
                <tbody>
                {foreach $mappingItems as $mappingItem}
                    <tr>
                        <td>{$mappingItem->carrier_wawi|htmlentities}</td>
                        <td>{__($mappingItem->carrier_paypal)|htmlentities}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <span class="btn btn-link px-2 mappings-edit" title="{__('modify')}"
                                      data-toggle="modal"
                                      data-target="#mappings-modal"
                                      data-carrier_wawi="{$mappingItem->carrier_wawi|htmlentities}"
                                      data-carrier_paypal="{$mappingItem->carrier_paypal|htmlentities}"
                                      data-id="{$mappingItem->id}">
                                    <span class="icon-hover">
                                        <span class="fal fa-edit"></span>
                                        <span class="fas fa-edit"></span>
                                    </span>
                                </span>
                                <button type="submit" name="task" value="deleteCarrierMapping"
                                        class="btn btn-link px-2 delete-confirm mappings-delete"
                                        title="{__('delete')}"
                                        data-toggle="tooltip"
                                        data-id="{$mappingItem->id}"
                                        data-modal-body="{__('Soll das Carrier-Mapping gelöscht werden?')}"
                                >
                                    <span class="icon-hover">
                                        <span class="fal fa-trash-alt"></span>
                                        <span class="fas fa-trash-alt"></span>
                                    </span>
                                </button>
                            </div>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        {else}
            <div class="alert alert-info">{__('Es wurden noch keine Carrier-Mappings angelegt')}</div>
        {/if}

        <div class="save-wrapper">
            <div class="row">
                <div class="mr-auto col-sm-6 col-xl-auto">
                    <button type="button"
                            class="btn btn-primary btn-block mappings-new"
                            data-toggle="modal"
                            data-target="#mappings-modal">
                        <i class="fal fa-save mr-0 mr-lg-2"></i> {__('Neues Mapping')}
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="mappings-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="edit-mapping" method="post">
                <div class="modal-header">
                    <h2 class="modal-title"></h2>
                </div>
                <div class="modal-body">
                    {$jtl_token}
                    <input type="hidden" name="kPlugin" value="{$kPlugin}" />
                    <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}" />
                    <input id="mappingID" type="hidden" name="id" value="" />

                    <div class="form-group form-row align-items-center" id="suggestWawiCarrier">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="carrier_wawi">{__('Carrier in JTL-Wawi')}:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <input type="text" id="carrier_wawi" name="carrier_wawi" value="" class="form-control" required>
                            <i class="fas fa-spinner fa-pulse typeahead-spinner"></i>
                        </div>
                        <script>
                            enableTypeahead('#carrier_wawi', 'jtl_ppc_carrier_mapping', 'cName', null, function(e, item) {
                                $('#carrier_wawi').val(item.cName);
                            }, $('#suggestWawiCarrier .fa-spinner'));
                        </script>
                    </div>
                    <div class="form-group form-row align-items-center">
                        <label class="col col-sm-4 col-form-label text-sm-right" for="carrier_paypal">{__('Carrier bei PayPal')}:</label>
                        <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2">
                            <select id="carrier_paypal" name="carrier_paypal" class="custom-select" required>
                                {foreach $paypalCarriers as $paypalCarrier}
                                    <option value="{$paypalCarrier}">
                                        {__($paypalCarrier)}
                                    </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="ml-auto col-sm-6 col-lg-auto mb-2">
                            <button type="button" class="btn btn-outline-primary btn-block" data-dismiss="modal">
                                {__('cancelWithIcon')}
                            </button>
                        </div>
                        <div class="col-sm-6 col-lg-auto ">
                            <button type="submit" class="btn btn-primary btn-block" name="task" value="saveCarrierMapping">
                                {__('saveWithIcon')}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    {literal}
    $(document).ready(function(){
        let $title = $('#mappings-modal .modal-title');
        $('.mappings-edit').on('click', function(){
            $title.html('{/literal} {__('Mapping bearbeiten')}{literal}');
            $('#carrier_wawi').val($(this).data('carrier_wawi'));
            $('#carrier_paypal').val($(this).data('carrier_paypal'));
            $('#mappingID').val($(this).data('id'));
        });
        $('.mappings-new').on('click', function(){
            $title.html('{/literal} {__('Mapping anlegen')}{literal}');
            $('#carrier_wawi').val('');
            $('#carrier_paypal').val('');
            $('#mappingID').val('');
        });
        $('.mappings-delete').on('click', function(){
            $('#deleteID').val($(this).data('id'));
        });
    });
    {/literal}
</script>
