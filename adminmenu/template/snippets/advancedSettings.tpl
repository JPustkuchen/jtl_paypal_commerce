<div id="advancedSetting_{$settingsName}" class="form-group form-row advancedSettings_{$setting['section']} align-items-center">
    <label class="col col-sm-4 col-form-label text-sm-right order-1" ></label>
    <div class="col-sm pl-sm-3 pr-sm-5 order-last order-sm-2 text-sm-left">
        <button type="button" class="ml-0 pl-0 mt-0 pt-0 btn btn-link advancedSettingsButton">
            {__('Erweiterte Einstellungen')}
            <i class="fa fa-chevron-down"></i>
        </button>
    </div>
</div>