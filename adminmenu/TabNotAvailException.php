<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use Exception;

/**
 * Class TabNotAvailException
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
class TabNotAvailException extends Exception
{
}
