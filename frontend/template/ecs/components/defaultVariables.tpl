let ppcConfig        = {$ppcConfig|json_encode},
ppcECSUrl            = '{$ppcECSUrl}',
errorMessage         = '{__('Error')}',
activeButtonLabel    = '{$ppcPreloadButtonLabelActive}',
ppcNamespace         = '{$ppcNamespace}',
wrapperID            = '#ppc-paypal-button-custom-' + ppcNamespace + '-wrapper',
buttonID             = '#ppc-paypal-button-' + ppcNamespace,
renderContainerID    = ppcConfig.layout === 'vertical'
? '#paypal-button-' + ppcNamespace  + '-container'
: '#ppc-' + ppcNamespace + '-horizontal-container',
spinnerID            = '#ppc-loading-spinner-express-' + ppcNamespace,
loadingPlaceholderID = '#ppc-loading-placeholder-' + ppcNamespace;
