<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use JTL\Plugin\PluginInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Configuration;

/**
 * Class AbstractPaymentFrontend
 * @package Plugin\jtl_paypal_commerce\frontend
 */
abstract class AbstractPaymentFrontend implements PaymentFrontendInterface
{
    /** @var PluginInterface */
    protected $plugin;

    /** @var PayPalPaymentInterface */
    protected $paymentMethod;

    /** @var JTLSmarty */
    protected $smarty;

    /** @var PayPalFrontend */
    protected $frontend;

    /**
     * AbstractPaymentFrontend constructor
     * @param PluginInterface        $plugin
     * @param PayPalPaymentInterface $paymentMethod
     * @param JTLSmarty              $smarty
     */
    public function __construct(PluginInterface $plugin, PayPalPaymentInterface $paymentMethod, JTLSmarty $smarty)
    {
        $this->plugin        = $plugin;
        $this->paymentMethod = $paymentMethod;
        $this->smarty        = $smarty;
        $this->frontend      = new PayPalFrontend($plugin, Shop::Container()->get(Configuration::class), $smarty);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentMethod(): PayPalPaymentInterface
    {
        return $this->paymentMethod;
    }

    /**
     * @inheritDoc
     */
    public function getPayPalFrontend(): PayPalFrontend
    {
        return $this->frontend;
    }
}
