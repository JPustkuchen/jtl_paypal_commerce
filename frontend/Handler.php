<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use DateTime;
use Exception;
use JTL\Alert\Alert;
use JTL\Checkout\Bestellung;
use JTL\Consent\Item;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Extensions\Upload\Upload;
use JTL\Helpers\Request;
use JTL\IO\IO;
use JTL\IO\IOError;
use JTL\IO\IOResponse;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\paymentmethod\PaymentmethodNotFoundException;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;

/**
 * Class Handler
 * @package Plugin\jtl_paypal_commerce\frontend
 */
final class Handler
{
    /** @var PluginInterface */
    private $plugin;

    /** @var DbInterface */
    private $db;

    /** @var Configuration */
    private $config;

    /**
     * Handler constructor.
     * @param PluginInterface    $plugin
     * @param DbInterface|null   $db
     * @param Configuration|null $configuration
     */
    public function __construct(PluginInterface $plugin, ?DbInterface $db = null, ?Configuration $configuration = null)
    {
        $this->plugin = $plugin;
        $this->db     = $db ?? Shop::Container()->getDB();
        $this->config = $configuration ?? Shop::Container()->get(Configuration::class);
    }

    /**
     * @param string $original
     * @return string
     */
    public static function getBackendTranslation(string $original): string
    {
        if (!Shop::isFrontend()) {
            return \__($original);
        }

        $getText   = Shop::Container()->getGetText();
        $oldLocale = $getText->getLanguage();
        $locale    = Helper::sanitizeLocale(Helper::getLocaleFromISO(Helper::sanitizeISOCode(Shop::Lang()->getIso())));
        if ($oldLocale !== $locale) {
            $getText->setLanguage($locale);
            $translate = \__($original);
            $getText->setLanguage($oldLocale);
        } else {
            $translate = \__($original);
        }

        return $translate;
    }

    /**
     * @return void
     */
    public function pageStepShipping(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_SHIPPING);
    }

    /**
     * @return void
     */
    public function pageStepPayment(): void
    {
        CheckoutPage::getInstance($this->plugin)->validatePayment(Shop::Smarty());
    }

    /**
     * @return void
     */
    public function pageStepConfirm(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_CONFIRM);
    }

    /**
     * @return void
     */
    public function pageStepAddress(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_ADDRESS);
    }

    /**
     * @return void
     */
    public function pageStepProductDetails(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::PRODUCT_DETAILS);
    }

    /**
     * @return void
     */
    public function pageStepCart(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::CART);
    }

    /**
     * @return void
     */
    public function pageCustomerAccount(): void
    {
        $orderId = Request::getInt('bestellung');
        if ($orderId > 0) {
            $order = new Bestellung($orderId);
            if (!empty($order->cPUIZahlungsdaten)) {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_INFO,
                    \nl2br($order->cPUIZahlungsdaten),
                    'paymentInformation'
                );
            }
        }
    }

    /**
     * @param PayPalPaymentInterface $payMethod
     * @param bool                   $timeout
     * @return object|null
     */
    private function getPaymentStateResult(PayPalPaymentInterface $payMethod, bool $timeout): ?object
    {
        $result = (object)[
            'state'    => null,
            'redirect' => null,
            'message'  => null,
            'timeout'  => $timeout,
        ];

        switch ($result->state = $payMethod->checkPaymentState()) {
            case OrderStatus::STATUS_CREATED:
                // Order is created and in process or approval... goto checkout
                $result->redirect = Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php');

                break;
            case OrderStatus::STATUS_PENDING_APPROVAL:
                // Order will be approved... waiting and check again
                $result->message = self::getBackendTranslation(
                    'Ihre Zahlung wird überprüft'
                );

                break;
            case OrderStatus::STATUS_APPROVED:
                // Order is approved and will be captured... waiting and check again
                $result->message = self::getBackendTranslation(
                    'Ihre Zahlung wurde genehmigt und die Bestellung wird jetzt erfasst'
                );

                break;
            case OrderStatus::STATUS_COMPLETED:
                $ppOrder = $payMethod->getPPOrder();
                if ($ppOrder !== null) {
                    $link = $ppOrder->getLink('paymentRedirect');
                    if ($link !== null) {
                        $result->redirect = $link;

                        break;
                    }
                }
                // Order is captured and will be finalized... waiting and check again
                $result->message = self::getBackendTranslation(
                    'Ihre Zahlung wurde erfasst und die Bestellung wird jetzt abgeschlossen'
                );

                break;
            case OrderStatus::STATUS_SAVED:
                $result->redirect = $payMethod->getReturnURL(new Bestellung());

                break;
            default:
                $result = null;
        }

        if ($timeout && $result !== null) {
            $payMethod->handlePaymentStateTimeout($result);
        }

        return $result;
    }

    /**
     * @param LinkInterface $link
     * @param JTLSmarty     $smarty
     * @return void
     */
    public function checkPaymentState(LinkInterface $link, JTLSmarty $smarty): void
    {
        $helper = Helper::getInstance($this->plugin);
        $cUID   = Request::getVar('uid');
        if ($cUID !== null) {
            $state = $this->db->queryPrepared(
                'SELECT tbestellstatus.kBestellung, tbestellung.kZahlungsart
                    FROM tbestellstatus
                    INNER JOIN tbestellung ON tbestellstatus.kBestellung = tbestellung.kBestellung
                    WHERE tbestellstatus.cUID = :cuid',
                ['cuid' => $cUID],
                ReturnType::SINGLE_OBJECT
            );
            if ($state) {
                $payMethod = $helper->getPaymentFromID((int)$state->kZahlungsart);
            } else {
                $payMethod = null;
            }
        } else {
            $payMethod = $helper->getPaymentFromID(Request::getInt('payment'))
                ?? $helper->getPaymentFromName('PayPalCommerce');
        }
        if ($payMethod === null) {
            Helper::redirectAndExit(Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1');
            exit();
        }

        $paymentState = $this->getPaymentStateResult($payMethod, Request::hasGPCData('timeout'));
        if ($paymentState === null) {
            Helper::redirectAndExit($payMethod->getReturnURL(new Bestellung()));
            exit();
        }
        if ($paymentState->redirect !== null) {
            Helper::redirectAndExit($paymentState->redirect);
            exit();
        }

        $link->setTitle($payMethod->getLocalizedPaymentName());
        $smarty->assign('waitingBackdrop', !$paymentState->timeout)
               ->assign('checkMessage', $paymentState->message)
               ->assign('methodID', $payMethod->getMethod()->getMethodID())
               ->assign('orderStateURL', $payMethod->getReturnURL(new Bestellung()));
    }

    /**
     * @param int  $methodID
     * @param bool $timeout
     * @return object
     */
    public function checkIOPaymentState(int $methodID, bool $timeout): object
    {
        $result    = new IOResponse();
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromID($methodID);
        if ($payMethod === null) {
            $result->setClientRedirect(
                Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1'
            );

            return $result;
        }

        $paymentState = $this->getPaymentStateResult($payMethod, $timeout);
        if ($paymentState === null) {
            $result->setClientRedirect($payMethod->getReturnURL(new Bestellung()));

            return $result;
        }
        if ($paymentState->redirect !== null) {
            $result->setClientRedirect($paymentState->redirect);
        } else {
            $result->assignDom('pp-loading-body span', 'innerHTML', $paymentState->message);
        }

        return $result;
    }

    /**
     * @param string      $fundingSource
     * @param string|null $payment
     * @param string|null $bnCode
     * @param array|null  $formData
     * @return object
     */
    public function createIOPPOrder(
        string $fundingSource,
        ?string $payment = null,
        ?string $bnCode = null,
        ?array $formData = null
    ): object {
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromName($payment ?? 'PayPalCommerce');
        if ($payMethod === null) {
            return new IOError('Paymentmethod not found');
        }

        $payMethod->unsetCache();
        $payMethod->setFundingSource($fundingSource);
        $result = new IOResponse();

        require_once \PFAD_ROOT . \PFAD_INCLUDES . 'bestellvorgang_inc.php';
        if (($formData !== null)
            && ((int)($formData['versandartwahl'] ?? 0) === 1 || (int)($formData['Versandart'] ?? 0) > 0)
            && !\pruefeVersandartWahl((int)$formData['Versandart'], $formData)
        ) {
            $alertSrvc = Shop::Container()->getAlertService();
            $alert     = $alertSrvc->getAlert('fillShipping');
            if ($alert !== null) {
                $alertSrvc->removeAlertByKey('fillShipping');
                $result->assignVar('createResult', $alert->getMessage());
            } else {
                $result->assignVar('createResult', Shop::Lang()->get('fillShipping', 'checkout'));
            }

            return $result;
        }
        $ppcOrderId = $payMethod->createPPOrder(
            Frontend::getCustomer(),
            Frontend::getCart(),
            $fundingSource,
            AppContext::SHIPPING_FROM_FILE,
            AppContext::PAY_CONTINUE,
            $bnCode ?? MerchantCredentials::BNCODE_EXPRESS
        );
        if ($ppcOrderId === null) {
            $logger = new Logger(Logger::TYPE_PAYMENT, $payMethod);
            $logger->write(\LOGLEVEL_NOTICE, 'createIOPPOrder - PayPal order can not be created.');

            $alertService = Shop::Container()->getAlertService();
            $alert        = $alertService->getAlert('createOrderRequest');
            if ($alert !== null) {
                $result->assignVar('createResultDetails', $alert->getMessage());
                unset($_SESSION['alerts']['createOrderRequest']);
            }
            $result->assignVar(
                'createResult',
                self::getBackendTranslation('Die Zahlung konnte nicht bei PayPal angefragt werden')
            );
        } else {
            $result->assignVar('orderId', $ppcOrderId);
        }

        return $result;
    }

    /**
     * @return void
     */
    public function handleECSOrder(): void
    {
        $linkHelper = Shop::Container()->getLinkService();
        $payMethod  = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        try {
            if ($payMethod === null) {
                throw new PaymentmethodNotFoundException('Paymentmethod PayPalCommerce not found');
            }
            $expressCheckout = new ExpressCheckout($payMethod, $this->config);
            if (!$expressCheckout->ecsCheckout()) {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_NOTE,
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_ecs_missing_data'),
                    'missingPayerData',
                    [
                        'saveInSession' => true,
                        'linkText'      => $payMethod->getLocalizedPaymentName(),
                    ]
                );
                Helper::redirectAndExit(
                    $linkHelper->getStaticRoute('bestellvorgang.php')
                    . ((int)Frontend::getCustomer()->kKunde === 0 ? '?unreg_form=1' : '')
                );
                exit();
            }

            Helper::redirectAndExit($linkHelper->getStaticRoute('bestellvorgang.php'));
            exit();
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                self::getBackendTranslation($e->getMessage()),
                'ppcNotFound',
                ['saveInSession' => true]
            );

            Helper::redirectAndExit($linkHelper->getStaticRoute('warenkorb.php'));
            exit();
        }
    }

    /**
     * @param array $args
     * @return void
     * @throws CircularReferenceException | ServiceNotFoundException | PaymentmethodNotFoundException
     */
    public function smarty(array $args): void
    {
        $checkoutPage = CheckoutPage::getInstance($this->plugin);
        /** @var JTLSmarty $smarty */
        $smarty = $args['smarty'];

        if ($checkoutPage->getPageStep() !== CheckoutPage::STEP_UNKNOWN) {
            $checkoutPage->render($smarty);
        } else {
            $payment  = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
            $frontend = new PayPalFrontend($this->plugin, Shop::Container()->get(Configuration::class), $smarty);
            if ($payment === null) {
                throw new PaymentmethodNotFoundException('Paymentmethod PayPalCommerce not found');
            }
            if ($payment->isValid(Frontend::getCustomer(), Frontend::getCart())) {
                $frontend->preloadInstalmentBannerJS(CheckoutPage::PAGE_SCOPE_MINICART);
                $frontend->preloadECSJS(CheckoutPage::PAGE_SCOPE_MINICART);
                $components = $frontend->renderMiniCartComponents(
                    $payment,
                    [],
                    Frontend::getCustomer(),
                    Frontend::getCart()
                );
                $frontend->renderPayPalJsSDK($components, $payment->getBNCode(), false, true);
            }
        }
    }

    /**
     * @param array $args
     */
    public function ioRequest(array $args): void
    {
        /** @var IO $io */
        $io = $args['io'];
        try {
            $io->register('jtl_paypal_commerce.checkPaymentState', [$this, 'checkIOPaymentState']);
            $io->register('jtl_paypal_commerce.createOrder', [$this, 'createIOPPOrder']);
        } catch (Exception $e) {
            $logger = new Logger(Logger::TYPE_INFORMATION);
            $logger->write(
                \LOGLEVEL_ERROR,
                $this->plugin->getPluginID() . '::ioRequest - can not register io handler (' . $e->getMessage() . ')'
            );
        }
    }

    /**
     * @param array $args
     */
    public function addConsentItem(array $args): void
    {
        $lastID   = $args['items']->reduce(static function ($result, Item $item) {
                $value = $item->getID();
                return $result === null || $value > $result ? $value : $result;
        }) ?? 0;
        $locale   = $this->plugin->getLocalization();
        $cmActive = Shop::getSettingValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        if ($cmActive === 'Y' &&
            $this->config->getPrefixedConfigItem(
                BackendUIsettings::BACKEND_SETTINGS_SECTION_CONSENTMANAGER . '_activate'
            ) === 'Y'
        ) {
            $langISO = Shop::getLanguageCode();
            $item    = new Item();
            $item->setName(self::getBackendTranslation('PayPal Express Checkout und Ratenzahlung'));
            $item->setID(++$lastID);
            $item->setItemID(Configuration::CONSENT_ID);
            $item->setDescription($locale->getTranslation(
                'jtl_paypal_commerce_instalment_banner_consent_description',
                $langISO
            ));
            $item->setPurpose($locale->getTranslation(
                'jtl_paypal_commerce_instalment_banner_consent_purpose',
                $langISO
            ));
            $item->setPrivacyPolicy(
                'https://www.paypal.com/de/webapps/mpp/ua/privacy-full?locale.x=' .
                Helper::sanitizeLocale(Helper::getLocaleFromISO(Helper::sanitizeISOCode(Shop::Lang()->getIso())))
            );
            $item->setCompany('PayPal');
            $args['items']->push($item);
        }
    }

    /**
     * @param array $args
     * @return void
     */
    public function saveOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order     = $args['oBestellung'];
        $helper    = Helper::getInstance($this->plugin);
        $payMethod = $helper->getPaymentFromID((int)$order->kZahlungsart);
        if ($payMethod !== null) {
            $payMethod->finalizeOrderInDB($order);
        }
    }

    /**
     * @param array $args
     * @return void
     * @throws Exception
     */
    public function updateOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order = $args['oBestellung'];
        $wawi  = $args['oBestellungWawi'];
        $sent  = empty($wawi->dVersandt) ? null : new DateTime($wawi->dVersandt);
        $now   = new DateTime();

        if (empty($wawi->cIdentCode)
            || $sent === null
            || $now->diff($sent)->format('%a') > \BESTELLUNG_VERSANDBESTAETIGUNG_MAX_TAGE
        ) {
            return;
        }
        $paymentIds = [];
        foreach ($this->plugin->getPaymentMethods()->getMethods() as $method) {
            $paymentIds[] = $method->getMethodID();
        }
        $payments = $this->db->getCollection(
            'SELECT tbestellung.kBestellung, COALESCE(cMap.carrier_paypal, tbestellung.cLogistiker) AS carrier,
                    tzahlungseingang.cHinweis AS transaction_id,
                    tbestellung.dVersandDatum AS shipmentDate,
                    ADDDATE(tbestellung.dVersandDatum, INTERVAL tversandart.nMaxLiefertage DAY) AS deliveryDate
                FROM tbestellung
                INNER JOIN tzahlungseingang
                    ON tzahlungseingang.kBestellung = tbestellung.kBestellung
                LEFT JOIN xplugin_jtl_paypal_checkout_carrier_mapping AS cMap
                    ON cMap.carrier_wawi = tbestellung.cLogistiker
                INNER JOIN tversandart
                    ON tversandart.kVersandart = tbestellung.kVersandart
                WHERE tbestellung.kBestellung = :orderId
                    AND tbestellung.kZahlungsart IN (' . \implode(', ', $paymentIds) . ")
                    AND tzahlungseingang.cHinweis RLIKE '^[A-Z0-9]+$'",
            [
                'orderId' => $order->kBestellung,
            ]
        );
        foreach ($payments as $payment) {
            $this->db->upsert('xplugin_jtl_paypal_checkout_shipment_state', (object)[
                'transaction_id' => $payment->transaction_id,
                'tracking_id'    => $wawi->cIdentCode,
                'carrier'        => $payment->carrier,
                'shipment_date'  => $payment->shipmentDate,
                'delivery_date'  => $payment->deliveryDate,
                'status_sent'    => 0,
            ], ['transaction_id']);
        }
    }

    /**
     * @param array $args
     * @return void
     */
    public function updateSession(array $args): void
    {
        /** @var Bestellung $order */
        $order     = $args['oBestellung'];
        $helper    = Helper::getInstance($this->plugin);
        $payMethod = $helper->getPaymentFromID((int)$order->kZahlungsart);
        if ($payMethod !== null) {
            $ppOrder = $payMethod->getPPOrder();
            if ($ppOrder !== null) {
                // Workaround for SHOP-6480 - should be removed if issue is fixed
                Upload::speicherUploadDateien(Frontend::getCart(), $order->kBestellung);

                $payMethod->updatePaymentState($payMethod->getPPHash($ppOrder), $order);
            }
        }
    }
}
