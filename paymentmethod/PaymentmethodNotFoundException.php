<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

/**
 * Class PaymentmethodNotFoundException
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
class PaymentmethodNotFoundException extends \Exception
{
}
